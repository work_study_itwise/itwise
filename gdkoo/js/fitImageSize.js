function fitImageSize(obj, href, maxWidth, maxHeight) {
  var image = new Image();

  image.onload = function(){

    var width = image.width;
    var height = image.height;
 
    var scalex = maxWidth / width;
    var scaley = maxHeight / height;

    var scale = (scalex < scaley) ? scalex : scaley;
    if (scale > 1)
        scale = 1;

    obj.width = scale * width;
    obj.height = scale * height;

    obj.style.display = "";
  }
  image.src = href;
}
